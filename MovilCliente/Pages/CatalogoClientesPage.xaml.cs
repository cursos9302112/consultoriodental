using BIZ;
using COMMON.Entidades;

namespace MovilCliente.Pages;

public partial class CatalogoClientesPage : ContentPage
{
	PacienteManager pacienteManager;
	List<string> filtros;
	List<Paciente> pacientes;
	public CatalogoClientesPage()
	{
		InitializeComponent();
		pacienteManager=FabricManager.PacienteManager();
		filtros = new List<string>();
		filtros.Add("Mujeres");
		filtros.Add("Hombres");
		filtros.Add("Nombre");
		filtros.Add("Apellidos");
		filtros.Add("CURP");
		filtros.Add("Todos");

		pkrFiltro.ItemsSource = filtros;
		
	}

    protected override void OnAppearing()
    {
        base.OnAppearing();
		pacientes= pacienteManager.ObtenerTodos;
        lstPacientes.ItemsSource = null;
        lstPacientes.ItemsSource = pacientes;
    }

    private void btnBuscar_Clicked(object sender, EventArgs e)
	{
		lstPacientes.ItemsSource = null;
		string filtro = pkrFiltro.SelectedItem as string;
		switch (filtro)
		{
			case "Todos":
				lstPacientes.ItemsSource = pacientes;
				break;
			case "Mujeres":
				lstPacientes.ItemsSource = pacientes.Where(p => p.Genero == 'F');
				break;
            case "Hombres":
                lstPacientes.ItemsSource = pacientes.Where(p => p.Genero == 'M');
                break;

            case "CURP":
                lstPacientes.ItemsSource = pacientes.Where(p => p.CURP.ToLower().Contains(entFiltro.Text.ToLower()) );
                break;
            case "Nombre":
                lstPacientes.ItemsSource = pacientes.Where(p => p.Nombre.ToLower().Contains(entFiltro.Text.ToLower()));
                break;
            case "Apellidos":
                lstPacientes.ItemsSource = pacientes.Where(p => p.Apellidos.ToLower().Contains(entFiltro.Text.ToLower()));
                break;
            default:
				lstPacientes.ItemsSource = pacientes;
				break;
		}

	}

    private void btnNuevo_Clicked(object sender, EventArgs e)
    {
		Navigation.PushAsync(new EditarPaciente(new Paciente()));
    }

    private void lstPacientes_ItemTapped(object sender, ItemTappedEventArgs e)
    {
		Navigation.PushAsync(new EditarPaciente(e.Item as Paciente));
    }
}