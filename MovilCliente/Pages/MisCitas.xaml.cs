using COMMON.Modelos;

namespace MovilCliente.Pages;

public partial class MisCitas : ContentPage
{
    List<CitasListadasModel> citas = new List<CitasListadasModel>();
    public MisCitas()
    {
        InitializeComponent();
        citas.Add(new CitasListadasModel()
        {
            SeCompletoLaCita = false,
            SeRequiereSegimiento = false,
            CitaGeneradaPorPaciente = true,
            FechaHora = DateTime.Now,
            IdCita = 21,
            IdDoctor = 21,
            IdPaciente = 22,
            IdPacienteQueGeneroCita = 22,
            IdPersonaQueGeneroCita = 0,
            NombreDelDoctor = "German Cuevas",
            Observaciones = "",
            Procedimiento = "",
            RetroalimentacionDelPaciente = "",
            Tratamiento = ""
        });

    }

    protected override void OnAppearing()
    {
        lstCitas.ItemsSource = null;
        lstCitas.ItemsSource = citas;

    }
}