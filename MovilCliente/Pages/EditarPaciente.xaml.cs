using BIZ;
using COMMON.Entidades;

namespace MovilCliente.Pages;

public partial class EditarPaciente : ContentPage
{
	Paciente paciente;
	bool esEdicion = false;
	List<string> generos;
	PacienteManager manager;
	public EditarPaciente(Paciente paciente)
	{
		InitializeComponent();
		manager = FabricManager.PacienteManager();
		this.paciente = paciente;
		esEdicion = paciente.IdPaciente != 0;
		BindingContext = paciente;
		generos = new List<string>();
		if (esEdicion)
		{
			Title = $"Editar a {paciente.Nombre} {paciente.Apellidos}";
			btnEliminar.IsVisible = true;
			pkrGenero.SelectedItem = paciente.Genero == 'M' ? "Masculino" : "Femenino";
		}
		else
		{
			Title = "Nuevo paciente";
			btnEliminar.IsVisible = false;
		}
		generos.Add("Femenino");
		generos.Add("Masculino");
		pkrGenero.ItemsSource= generos;
	}

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
		paciente.Genero = (pkrGenero.SelectedItem as string) == "Masculino" ? 'M' : 'F';
		Paciente editado=esEdicion?manager.Actualizar(paciente,paciente.IdPaciente):manager.Insertar(paciente);
		if(editado != null )
		{
			DisplayAlert("Exito", "Registro correctamente almacenado", "Ok");
			Navigation.PopAsync();
		}
		else
		{
            DisplayAlert("Error", manager.Error, "Ok");
        }
    }

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
		if (DisplayAlert("Confirma", $"�Deseas eliminar a {paciente.Nombre} {paciente.Apellidos}?", "S�", "No").Result)
		{
			if (manager.Eliminar(paciente.IdPaciente))
			{
				DisplayAlert("Paciente", "Eliminado correctamente", "Ok");
				Navigation.PopAsync();
			}
			else
			{
				DisplayAlert("Error", manager.Error, "Ok");
			}
		}
    }
}