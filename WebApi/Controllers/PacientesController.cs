﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacientesController : GenericController<Paciente>
    {
        public PacientesController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
