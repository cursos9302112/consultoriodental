﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPersonalController : GenericController<TipoPersonal>
    {
        public TipoPersonalController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
