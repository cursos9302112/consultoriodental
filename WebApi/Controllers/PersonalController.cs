﻿using COMMON.Entidades;
using COMMON.Modelos;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalController : GenericController<Personal>
    {
        public PersonalController(FabricRepository fabric) : base(fabric)
        {
        }
        [HttpGet("ObtenerReporteDePersonal")]
        public ActionResult<List<ReportePersonalModel>> ObtenerReporteDePersonal()
        {
            var datos= repo.Query<ReportePersonalModel>("select * from ViewReportePersonal for JSON AUTO");
            if (datos != null)
            {
                return Ok(datos);
            }
            else
            {
                return BadRequest(repo.Error);
            }
        }
    }
}
