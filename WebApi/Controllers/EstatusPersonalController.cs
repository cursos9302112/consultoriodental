﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstatusPersonalController : GenericController<EstatusPersonal>
    {
        public EstatusPersonalController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
