﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitasController : GenericController<Cita>
    {
        public CitasController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
