﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class TipoPersonal
    {
        public int IdTipoPersonal { get; set; }
        public string Rol { get; set; }
    }
}
