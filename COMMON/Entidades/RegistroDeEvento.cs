﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class RegistroDeEvento
    {
        public int IdRegistro { get; set; }
        public DateTime FechaHora { get; set; }
        public string CURP { get; set; }
        public string Descripcion { get; set; }
        public string ConsultaSQL { get; set; }
        public string Tabla { get; set; }
    }
}
