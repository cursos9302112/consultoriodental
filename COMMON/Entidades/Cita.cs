﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Cita
    {
        public int IdCita { get; set; }
        public int IdDoctor { get; set; }
        public int IdPaciente { get; set; }
        public bool CitaGeneradaPorPaciente { get; set; }
        public int IdPersonaQueGeneroCita { get; set; }
        public int IdPacienteQueGeneroCita { get; set; }
        public DateTime FechaHora { get; set; }
        public string Procedimiento { get; set; }
        public string Observaciones { get; set; }
        public string Tratamiento { get; set; }
        public bool SeRequiereSegimiento { get; set; }
        public bool SeCompletoLaCita { get; set; }
        public string RetroalimentacionDelPaciente { get; set; }
    }
}
