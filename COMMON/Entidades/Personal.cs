﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Personal:Persona
    {
        public int IdPersonal { get; set; }
        public int TipoPersonal { get; set; }
        public string GradoDeEstudios { get; set; }
        public string CedulaProfecional { get; set; }
        public int IdEstatus { get; set; }
        
    }
}
