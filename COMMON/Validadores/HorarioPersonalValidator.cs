﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class HorarioPersonalValidator:AbstractValidator<HorarioPersonal>
    {
        public HorarioPersonalValidator()
        {
            RuleFor(h => h.HoraSalida).NotEmpty();
            RuleFor(h => h.HoraEntrada).NotEmpty();
            RuleFor(h=>h.IdPersonal).NotEmpty();
            RuleFor(h=>h.NumDiaSemana).NotEmpty().GreaterThanOrEqualTo(1).LessThanOrEqualTo(7);
           
        }
    }
}
