﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class PersonalValidator:PersonaValidator<Personal>
    {
        public PersonalValidator()
        {
            RuleFor(p => p.TipoPersonal).NotEmpty();
            RuleFor(p=>p.GradoDeEstudios).NotEmpty().MaximumLength(150);
            RuleFor(p=>p.CedulaProfecional).NotEmpty().MaximumLength(50);
            RuleFor(p => p.IdEstatus).NotEmpty();
        }
    }
}
