﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public abstract class PersonaValidator<T> : AbstractValidator<T> where T : Persona
    {
        public PersonaValidator()
        {
            RuleFor(p => p.Apellidos).NotEmpty().MinimumLength(3).MaximumLength(50);
            RuleFor(p => p.Nombre).NotEmpty().MaximumLength(50).MinimumLength(3);
            RuleFor(p => p.Genero).NotEmpty();
            RuleFor(p => p.CURP).NotEmpty().MinimumLength(10).MaximumLength(50);
            RuleFor(p => p.Telefono).NotEmpty().MinimumLength(10).MaximumLength(50);
            RuleFor(p => p.Email).NotEmpty().EmailAddress().MaximumLength(50);
            RuleFor(p => p.Password).NotEmpty().MinimumLength(8).MaximumLength(50);
        }
    }
}
