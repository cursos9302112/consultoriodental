﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class PacienteValidador:PersonaValidator<Paciente>
    {
        public PacienteValidador()
        {
            // RuleFor(p => p.IdPaciente).NotEmpty();//Dejar esta linea si el Id es de tipo cadena o quitar cuando el Id es autonumérico
            RuleFor(p => p.Domicilio).NotEmpty().MaximumLength(250);
            RuleFor(p => p.TelEmergencia).NotEmpty().MaximumLength(50).MinimumLength(10);
            RuleFor(p => p.ContactoDeEmergencia).NotEmpty().MinimumLength(5).MaximumLength(50);
            //NOTA: los campos que pueden ser nulos NO es necesario validar
        }
    }
}
