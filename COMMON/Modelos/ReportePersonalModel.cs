﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ReportePersonalModel
    {
        public int IdPersonal { get; set; }
        public string Rol { get; set; }
        public string Estatus { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public char Genero { get; set; }
        public string CURP { get; set; }
        public string GradoDeEstudios { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
    }
}
