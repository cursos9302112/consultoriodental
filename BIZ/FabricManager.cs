﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public static class FabricManager
    {
        static string UrlBase= @"https://webapicurso.azurewebsites.net";
        

        public static CitasManager CitasManager() => new CitasManager(UrlBase, "Citas");
        public static EstatusPersonalManager EstatusPersonalManager() => new EstatusPersonalManager(UrlBase, "EstatusPersonal");
        public static HorarioPersonalManager HorarioPersonalManager() => new HorarioPersonalManager(UrlBase, "HorariosPersonal");
        public static PacienteManager PacienteManager() => new PacienteManager(UrlBase, "Pacientes");
        public static PersonalManager PersonalManager() => new PersonalManager(UrlBase, "Personal");
        public static ResgitroDeEventosManager ResgitroDeEventosManager() => new ResgitroDeEventosManager(UrlBase, "RegistroDeEvento");
        public static TipoPersonalManager TipoPersonalManager() => new TipoPersonalManager(UrlBase, "Personal");

    }
}
