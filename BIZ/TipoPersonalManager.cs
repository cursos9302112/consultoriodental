﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class TipoPersonalManager : GenericManager<TipoPersonal>
    {
        public TipoPersonalManager(string urlBase, string tabla) : base(urlBase, tabla)
        {
        }
    }
}
