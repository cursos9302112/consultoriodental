﻿using COMMON.Entidades;
using COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class PersonalManager : GenericManager<Personal>
    {
        public PersonalManager(string urlBase, string tabla) : base(urlBase, tabla)
        {
        }

        public List<ReportePersonalModel> ObternerReporteDePersonal()
        {
            return ObtenerReporteDePersonalAsync().Result;
        }

        private async Task<List<ReportePersonalModel>> ObtenerReporteDePersonalAsync()
        {
            HttpResponseMessage response = await HttpClient.GetAsync($"{UrlBase}/api/{Tabla}/ObtenerReporteDePersonal").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ReportePersonalModel>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }
    }
}
